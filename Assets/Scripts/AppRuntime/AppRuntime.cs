﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class AppRuntime : BaseRuntime
{
    void Awake()
    {
        if (Singleton.Instance == null)
        {
            Singleton.Instance = gameObject.AddComponent<Singleton>();
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        MakeFSM();
    }

    protected override void MakeFSM()
    {
        LoginState loginState = new LoginState();
        loginState.AddTRANSITION(Transition.TRANSITION_TO_REGISTER, StateID.REGISTER);
        loginState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);

        RegisterState registerState = new RegisterState();
        registerState.AddTRANSITION(Transition.TRANSITION_TO_LOGIN, StateID.LOGIN);

        ARState arState = new ARState();
        arState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);
        arState.AddTRANSITION(Transition.TRANSITION_TO_CHECKOUT, StateID.CHECKOUT);

        HomeState homeState = new HomeState();
        homeState.AddTRANSITION(Transition.TRANSITION_TO_LOGIN, StateID.LOGIN);
        homeState.AddTRANSITION(Transition.TRANSITION_TO_AR, StateID.AR);
        homeState.AddTRANSITION(Transition.TRANSITION_TO_ACCOUNT, StateID.ACCOUNT);

        CheckoutState checkoutState = new CheckoutState();
        checkoutState.AddTRANSITION(Transition.TRANSITION_TO_AR, StateID.AR);
        checkoutState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);

        AkunState akunState = new AkunState();
        akunState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);

        _FSM = new FSMSystem(this);
        _FSM.AddState(loginState);
        _FSM.AddState(registerState);
        _FSM.AddState(arState);
        _FSM.AddState(homeState);
        _FSM.AddState(checkoutState);
        _FSM.AddState(akunState);

        base.MakeFSM();
    }
}
