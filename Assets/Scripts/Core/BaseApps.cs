﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace BaseApps
{
    public struct CONST_VAR
    {
        private const string SERVER_URL =
                                            //"http://serverlocal.com/dekorasi/";
                                            "http://instiecplanner.com/api_app/";

        //user
        private const string USER = SERVER_URL + "user/";
        public const string GET_USER = USER + "getUser.php";
        public const string REGISTER_USER = USER + "registerUser.php";

        //item
        private const string ITEM = SERVER_URL + "item/";
        public const string GET_ITEM = ITEM + "getItems.php";
        public const string ADD_TO_CART = ITEM + "addToCart.php";
        public const string DELETE_CART = ITEM + "deleteFromCart.php";
        public const string GET_CART = ITEM + "getCart.php";

        //order
        private const string ORDER = SERVER_URL + "order/";
        public const string CONFIRM_PAY = ORDER + "confirmPay.php";

        public const string WHATSAPP = "whatsapp://send?phone=6281905076404";
    }

    public enum Transition
    {
        NullTransition = 0,
        TRANSITION_TO_LOGIN = 1,
        TRANSITION_TO_REGISTER = 2,
        TRANSITION_TO_HOME = 3,
        TRANSITION_TO_AR = 4,
        TRANSITION_TO_CHECKOUT = 5,
        TRANSITION_TO_ACCOUNT = 6,
    }

    public enum StateID
    {
        NullStateID = 0,
        LOGIN = 1,
        REGISTER = 2,
        HOME = 3,
        AR = 4,
        CHECKOUT = 5,
        ACCOUNT = 6,
    }

    public enum TypeDecoration
    {
        NullDecoration = 0,
        TABLE_SETTING = 1,
        TABLE_STAGE = 2,
        BEDROOM = 3,
        PHOTOBOOTH = 4,
        AQIQAH = 5,
        BACKDROP = 6,
    }
}
