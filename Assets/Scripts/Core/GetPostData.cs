﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class GetPostData 
{
    public static WWW RegisterUser(string username, string password, string name, string address, string email, string phone)
    {
        WWWForm insertUser = new WWWForm();

        insertUser.AddField("username", username);
        insertUser.AddField("password", password);
        insertUser.AddField("name", name);
        insertUser.AddField("address", address);
        insertUser.AddField("email", email);
        insertUser.AddField("phone", phone);

        WWW www = new WWW(CONST_VAR.REGISTER_USER, insertUser);
        return www;
    }

    public static WWW GetUser(string username, string password)
    {
        WWWForm getUser = new WWWForm();

        getUser.AddField("username", username);
        getUser.AddField("password", password);

        WWW www = new WWW(CONST_VAR.GET_USER, getUser);
        return www;
    }

    public static WWW ConfirmPay(string name, string phone, string address, string email, int totalPrice, string bank)
    {
        WWWForm confirmPay = new WWWForm();
        
        confirmPay.AddField("username", Singleton.Instance.Username);
        confirmPay.AddField("name", name);
        confirmPay.AddField("phone", phone);
        confirmPay.AddField("address", address);
        confirmPay.AddField("email", email);
        confirmPay.AddField("bank", bank);
        confirmPay.AddField("total_price", totalPrice);

        WWW www = new WWW(CONST_VAR.CONFIRM_PAY, confirmPay);
        return www;
    }

    public static WWW AddToCart(string product)
    {
        WWWForm addToCart = new WWWForm();

        addToCart.AddField("username", Singleton.Instance.Username);
        addToCart.AddField("product_name", product);

        WWW www = new WWW(CONST_VAR.ADD_TO_CART, addToCart);
        return www;
    }

    public static WWW DeleteCart(string product)
    {
        WWWForm deleteCart = new WWWForm();
        deleteCart.AddField("username", Singleton.Instance.Username);
        deleteCart.AddField("product_name", product);

        WWW www = new WWW(CONST_VAR.DELETE_CART, deleteCart);
        return www;
    }

    public static WWW GetCart()
    {
        WWWForm addToCart = new WWWForm();
        addToCart.AddField("username", Singleton.Instance.Username);

        WWW www = new WWW(CONST_VAR.GET_CART, addToCart);
        return www;
    }
}
