﻿using BaseApps;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemClass
{

}

public class ItemProductClass : ItemClass
{
    public string ProductName { get; set; }
    public string Detail { get; set; }
    public int Price { get; set; }

    public ItemProductClass(string product_name, string detail, int price)
    {
        ProductName = product_name;
        Detail = detail;
        Price = price;
    }
}