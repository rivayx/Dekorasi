﻿using System;
using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public class GetItems : JSONParsing
{
    public GetItems(string JSONPath) : base(JSONPath) { }

    protected override List<ItemClass> JSONObjectToItems(JSONNode jsonNode)
    {
        List<ItemClass> itemClass = new List<ItemClass>();

        for (int i = 0; i < jsonNode["data"].Count; i++)
        {
            JSONNode node = jsonNode["data"][i];

            string product_name = node["product_name"];
            string detail = node["detail"];
            int price = node["price"];

            ItemClass item = new ItemProductClass(product_name, detail, price);
            itemClass.Add(item);
        }
        return itemClass;
    }
}