﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseApps;
using SimpleJSON;

public class Singleton : MonoBehaviour
{
    public string Username { get; set; }
    public string FullName { get; set; }
    public string Address { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }

    public int TotalPrice { get; set; }

    public List<ItemProductClass> ListProduct = new List<ItemProductClass>();
    public List<ItemProductClass> ListProductInCart = new List<ItemProductClass>();

    public TypeDecoration Decoration { get; set; }

    public static Singleton Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            if (Instance == null)
            {
                Instantiate(Instance);
                Debug.Log("error when trying to create singleton");
            }
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


}
