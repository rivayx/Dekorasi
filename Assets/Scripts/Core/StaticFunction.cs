﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using System.Globalization;

public class StaticFunction
{
    public static string FormatCurrency(int value)
    {
        return "Rp " + FormatGeneral(value);
    }

    public static string FormatGeneral(int value)
    {
        return FormatNumber("N0", value);
    }

    public static string FormatNumber(string format, int value)
    {
        CultureInfo id = new CultureInfo("id-ID");

        string val = value.ToString(format, id);

        return val;
    }

    public static TypeDecoration ConvertTypeIntToDecoration(int val)
    {
        TypeDecoration decoration = TypeDecoration.NullDecoration;

        switch (val)
        {
            case 0:
                Debug.LogError("Type Decoration is Null");
                break;
            case 1:
                decoration = TypeDecoration.TABLE_SETTING;
                break;
            case 2:
                decoration = TypeDecoration.TABLE_STAGE;
                break;
            case 3:
                decoration = TypeDecoration.BEDROOM;
                break;
            case 4:
                decoration = TypeDecoration.PHOTOBOOTH;
                break;
            case 5:
                decoration = TypeDecoration.AQIQAH;
                break;
            case 6:
                decoration = TypeDecoration.BACKDROP;
                break;
            default:
                Debug.LogError("Type Decoration is Null");
                break;
        }

        return decoration;
    }
}