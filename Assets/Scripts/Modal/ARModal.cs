﻿using maxstAR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ARModal : BaseModal
{
    [SerializeField]
    private Button _ShowHolder;
    [SerializeField]
    private RectTransform _HolderItemButton;
    [SerializeField]
    private RectTransform _ShowPosition;
    [SerializeField]
    private RectTransform _HidePosition;
    [SerializeField]
    private Button _StopTracking;
    [SerializeField]
    private Button _Back;

    [SerializeField]
    private GameObject _SelectColor;
    [SerializeField]
    private Material _MaterialEdit;

    [SerializeField]
    private Button _Checkout;

    [SerializeField]
    private List<Button> _ButtonItemList;
    [SerializeField]
    private List<GameObject> _ObjectList;

    private Vector3 touchToWorldPosition = Vector3.zero;
    private Vector3 touchSumPosition = Vector3.zero;

    private string detailProduct;

    private bool startTrackerDone = false;
    private bool cameraStartDone = false;
    private bool findSurfaceDone = false;

    private bool showHolder = false;

    private InstantTrackableBehaviour instantTrackable = null;
    private CameraBackgroundBehaviour cameraBackgroundBehaviour = null;
    
    public UnityAction toHomeState;
    public UnityAction toCheckoutState;

    private static ARModal _Instance;
    public static ARModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<ARModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no ARModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _SelectColor.SetActive(false);
        _StopTracking.gameObject.SetActive(false);
        _HolderItemButton.sizeDelta = new Vector2(_HolderItemButton.sizeDelta.x, 0);

        foreach (Button item in _ButtonItemList)
        {
            item.gameObject.SetActive(false);

            if (item.GetComponent<ItemVariableGatherer>().Decoration == Singleton.Instance.Decoration)
            {
                _HolderItemButton.sizeDelta = new Vector2(_HolderItemButton.sizeDelta.x, _HolderItemButton.sizeDelta.y + 220);
                item.gameObject.SetActive(true);
                
                foreach (ItemProductClass product in Singleton.Instance.ListProduct)
                {
                    if(item.name == product.ProductName)
                    {
                        item.GetComponentInChildren<Text>().text = StaticFunction.FormatCurrency(product.Price);
                    }
                }
            }
        }

        cameraBackgroundBehaviour = FindObjectOfType<CameraBackgroundBehaviour>();
        if (cameraBackgroundBehaviour == null)
        {
            Debug.LogError("Can't find CameraBackgroundBehaviour.");
            return;
        }        
        instantTrackable = FindObjectOfType<InstantTrackableBehaviour>();
        if (instantTrackable == null)
        {
            return;
        }
        instantTrackable.OnTrackFail();

        _Back.onClick.AddListener(toHomeState);
        _ShowHolder.onClick.AddListener(ShowHolder);
    }

    private void ShowHolder()
    {
        showHolder = true;
    }

    public void ColorSelect(Image img)
    {
        _MaterialEdit.color = img.color;
    }

    protected override void Loop()
    {
        base.Loop();

        if (showHolder)
        {
            _HolderItemButton.position = Vector3.Lerp(_HolderItemButton.position, _ShowPosition.position, Time.deltaTime * 10);
        }
        else
        {
            _HolderItemButton.position = Vector3.Lerp(_HolderItemButton.position, _HidePosition.position, Time.deltaTime * 10);
        }

        if (instantTrackable == null)
        {
            return;
        }

        StartCamera();

        if (!startTrackerDone)
        {
            TrackerManager.GetInstance().StartTracker(TrackerManager.TRACKER_TYPE_INSTANT);
            SensorDevice.GetInstance().Start();
            startTrackerDone = true;
        }

        TrackingState state = TrackerManager.GetInstance().UpdateTrackingState();

        cameraBackgroundBehaviour.UpdateCameraBackgroundImage(state);

        TrackingResult trackingResult = state.GetTrackingResult();

        if (trackingResult.GetCount() == 0)
        {
            instantTrackable.OnTrackFail();
            return;
        }

        if (Input.touchCount > 0)
        {
            UpdateTouchDelta(Input.GetTouch(0).position);
        }

        Trackable trackable = trackingResult.GetTrackable(0);
        Matrix4x4 poseMatrix = trackable.GetPose() * Matrix4x4.Translate(touchSumPosition);
        instantTrackable.OnTrackSuccess(trackable.GetId(), trackable.GetName(), poseMatrix);
    }

    private void UpdateTouchDelta(Vector2 touchPosition)
    {
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                touchToWorldPosition = TrackerManager.GetInstance().GetWorldPositionFromScreenCoordinate(touchPosition);
                break;

            case TouchPhase.Moved:
                Vector3 currentWorldPosition = TrackerManager.GetInstance().GetWorldPositionFromScreenCoordinate(touchPosition);
                touchSumPosition += (currentWorldPosition - touchToWorldPosition);
                touchToWorldPosition = currentWorldPosition;
                break;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SensorDevice.GetInstance().Stop();
            TrackerManager.GetInstance().StopTracker();
            startTrackerDone = false;
            StopCamera();
        }
    }

    private void StartCamera()
    {
        if (!cameraStartDone)
        {
            Debug.Log("Unity StartCamera");
            ResultCode result = CameraDevice.GetInstance().Start();
            if (result == ResultCode.Success)
            {
                cameraStartDone = true;
                //CameraDevice.GetInstance().SetAutoWhiteBalanceLock(true);   // For ODG-R7 preventing camera flickering
            }
        }
    }

    private void StopCamera()
    {
        if (cameraStartDone)
        {
            Debug.Log("Unity StopCamera");
            CameraDevice.GetInstance().Stop();
            cameraStartDone = false;
        }
    }

    public void CanEditColor(bool val)
    {
        _SelectColor.SetActive(val);
    }

    public void OnClickStart(GameObject obj)
    {
        foreach (GameObject go in _ObjectList)
        {
            go.SetActive(false);
        }
        
        obj.SetActive(true);

        foreach (ItemProductClass item in Singleton.Instance.ListProduct)
        {
            if (item.ProductName == obj.name)
            {
                _Checkout.onClick.AddListener(() => StartCoroutine(AddToCart(item.ProductName)));
                showHolder = false;

                detailProduct = item.Detail;
                break;
            }
        }

        _StopTracking.gameObject.SetActive(true);
        _Checkout.gameObject.SetActive(true);

        _StopTracking.onClick.AddListener(StopTracking);

        if (!findSurfaceDone)
        {
            TrackerManager.GetInstance().FindSurface();
            touchSumPosition = Vector3.zero;
            findSurfaceDone = true;
        }
        else
        {

        }
    }

    private IEnumerator AddToCart(string productName)
    {
        WWW addToCart = GetPostData.AddToCart(productName);
        yield return addToCart;
        Debug.Log(addToCart.text);

        MessageModal.Instance().OpenMessage("Berhasil menambahkan Cart, silahkan klik 'OK' untuk Checkout atau 'Cancel' untuk menambahkan produk lain", toCheckoutState, toHomeState);
    }

    private void StopTracking()
    {
        _StopTracking.onClick.RemoveAllListeners();
        _Checkout.onClick.RemoveAllListeners();

        _StopTracking.gameObject.SetActive(false);
        _Checkout.gameObject.SetActive(false);

        TrackerManager.GetInstance().QuitFindingSurface();
        findSurfaceDone = false;
    }

    public override void CloseModal()
    {
        StopTracking();
        SensorDevice.GetInstance().Stop();
        TrackerManager.GetInstance().StopTracker();
        TrackerManager.GetInstance().DestroyTracker();
        StopCamera();

        _Back.onClick.RemoveAllListeners();
        _ShowHolder.onClick.RemoveAllListeners();

        base.CloseModal();
    }
}
