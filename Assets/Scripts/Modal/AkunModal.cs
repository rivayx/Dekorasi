﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AkunModal : BaseModal
{
    [SerializeField]
    private Button _BackButton;
    [SerializeField]
    private Text _FullName;
    [SerializeField]
    private Text _Phone;
    [SerializeField]
    private Text _Address;

    [SerializeField]
    private Transform _HolderItem;
    [SerializeField]
    private CartVariableGatherer _Item;

    private List<GameObject> ListItem = new List<GameObject>();

    private static AkunModal _Instance;
    public static AkunModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<AkunModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no AkunModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _FullName.text = Singleton.Instance.FullName;
        _Phone.text = Singleton.Instance.Phone;
        _Address.text = Singleton.Instance.Address;
        _Item.gameObject.SetActive(false);

        StartCoroutine(GetCart());
    }

    private IEnumerator GetCart()
    {
        Singleton.Instance.ListProductInCart.Clear();
        Singleton.Instance.TotalPrice = 0;

        WWW getCart = GetPostData.GetCart();
        yield return getCart;
        string jsonString = getCart.text;
        Debug.Log(getCart.text);

        JSONNode node = JSONNode.Parse(jsonString);

        if (jsonString == null || jsonString == "null")
            jsonString = "";

        if (jsonString != "")
        {
            for (int i = 0; i < node.Count; i++)
            {
                foreach (ItemProductClass product in Singleton.Instance.ListProduct)
                {
                    string x = node[i];
                    if (x == product.ProductName)
                    {
                        Singleton.Instance.ListProductInCart.Add(product);
                        Singleton.Instance.TotalPrice += product.Price;
                    }
                }
            }

            if (ListItem.Count != 0)
            {
                foreach (GameObject go in ListItem)
                {
                    Destroy(go);
                }
                ListItem.Clear();
            }

            StartCoroutine(CreateDynamicData());
        }
    }

    private IEnumerator CreateDynamicData()
    {

        foreach (ItemClass item in Singleton.Instance.ListProductInCart)
        {
            ItemProductClass itemCart = item as ItemProductClass;
            _Item.gameObject.SetActive(false);
            _Item.ProductName.text = itemCart.ProductName;
            _Item.Price.text = StaticFunction.FormatGeneral(itemCart.Price);
            GameObject go = Instantiate(_Item.gameObject);
            go.transform.SetParent(_HolderItem);
            go.transform.localScale = Vector3.one;
            go.name = itemCart.ProductName;
            go.SetActive(true);
            go.GetComponent<CartVariableGatherer>().Delete.onClick.AddListener(() => DeleteAction(itemCart.ProductName));
            ListItem.Add(go);
            yield return new WaitForEndOfFrame();
        }
    }

    private void DeleteAction(string product)
    {
        GetPostData.DeleteCart(product);
        OpenModal();
    }

    public void RegisterModal(UnityAction toHomeState)
    {
        _BackButton.onClick.AddListener(toHomeState);
    }

    public override void CloseModal()
    {
        _BackButton.onClick.RemoveAllListeners();

        base.CloseModal();
    }
}
