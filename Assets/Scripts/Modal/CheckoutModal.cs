﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CheckoutModal : BaseModal
{
    [SerializeField]
    private Button _Back;

    [SerializeField]
    private Text _UserName;
    [SerializeField]
    private RectTransform _PanelCheckout;
    [SerializeField]
    private Transform _SelectStep;
    [SerializeField]
    private Button _NextStep;

    [SerializeField]
    private List<Transform> _Title;

    //DATA
    [SerializeField]
    private InputField _Name;
    [SerializeField]
    private InputField _Phone;
    [SerializeField]
    private InputField _Address;
    [SerializeField]
    private InputField _Email;

    //PAYMENT
    [SerializeField]
    private Transform _HolnerItem;
    [SerializeField]
    private CartVariableGatherer _Item;
    [SerializeField]
    private Text _Total;
    [SerializeField]
    private Text _UniqueCode;
    [SerializeField]
    private Text _TotalPayment;
    [SerializeField]
    private Button _BNI;
    [SerializeField]
    private Button _MANDIRI;
    [SerializeField]
    private Text _NoRek;
    [SerializeField]
    private Text _AtasNama;

    //FINISH
    [SerializeField]
    private Text _Wording;

    public UnityAction toARState;
    public UnityAction toHomeState;

    private List<GameObject> ListItem = new List<GameObject>();

    private int uniqueCode;
    private int totalPayment;
    private string bank = "";
    private int step;

    private static CheckoutModal _Instance;
    public static CheckoutModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<CheckoutModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no CheckoutModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _Back.onClick.AddListener(toHomeState);
        step = 0;
        StartCoroutine(GetCart());

        _UserName.text = "Login as " + Singleton.Instance.Username;

        _NextStep.onClick.AddListener(NextStepAction);
        _BNI.onClick.AddListener(() => BankAction("BNI"));
        _MANDIRI.onClick.AddListener(() => BankAction("MANDIRI"));
        _Item.gameObject.SetActive(false);
    }

    private IEnumerator GetCart()
    {
        reload:

        Singleton.Instance.ListProductInCart.Clear();
        Singleton.Instance.TotalPrice = 0;
        WWW getCart = GetPostData.GetCart();
        yield return getCart;
        string jsonString = getCart.text;
        //Debug.Log(jsonString);

        if (jsonString == "null")
        {
            goto reload;
        }

        JSONNode node = JSONNode.Parse(jsonString);

        for (int i = 0; i < node.Count; i++)
        {
            foreach (ItemProductClass product in Singleton.Instance.ListProduct)
            {
                string x = node[i];
                if (x == product.ProductName)
                {
                    Singleton.Instance.ListProductInCart.Add(product);
                    Singleton.Instance.TotalPrice += product.Price;
                }
            }
        }

        if (ListItem.Count != 0)
        {
            foreach (GameObject go in ListItem)
            {
                Destroy(go);
            }
            ListItem.Clear();
        }

        StartCoroutine(CreateDynamicData());
    }

    private IEnumerator CreateDynamicData()
    {
        
        uniqueCode = Random.Range(100, 999);
        totalPayment = Singleton.Instance.TotalPrice + uniqueCode;
        _UniqueCode.text = uniqueCode.ToString();
        _Total.text = StaticFunction.FormatGeneral(Singleton.Instance.TotalPrice);
        _TotalPayment.text = StaticFunction.FormatGeneral(totalPayment);

        foreach (ItemClass item in Singleton.Instance.ListProductInCart)
        {
            ItemProductClass itemCart = item as ItemProductClass;
            _Item.gameObject.SetActive(false);
            _Item.ProductName.text = itemCart.ProductName;
            _Item.Price.text = StaticFunction.FormatGeneral(itemCart.Price);
            GameObject go = Instantiate(_Item.gameObject);
            go.transform.SetParent(_HolnerItem);
            go.transform.localScale = Vector3.one;
            go.name = itemCart.ProductName;
            go.SetActive(true);
            ListItem.Add(go);
            yield return new WaitForEndOfFrame();
        }

        _Wording.text = "Terimakasih " + _Name.text + ", pesanan Anda telah kami terima melalui akun " + Singleton.Instance.Username + ". /r/nSilahkan menunggu 1x24 jam untuk dihubungi oleh admin Instiec Planner. Terimakasih";
    }

    protected override void Loop()
    {
        base.Loop();

        float newPosX = Mathf.Lerp(_PanelCheckout.anchoredPosition.x, -_PanelCheckout.sizeDelta.x * step, Time.deltaTime * 8);
        _PanelCheckout.anchoredPosition = new Vector2(newPosX, _PanelCheckout.anchoredPosition.y);
        _SelectStep.position = Vector2.Lerp(_SelectStep.position, _Title[step].position, Time.deltaTime * 8);

        if (step == 0)
        {
            if (_Name.text == "" || _Phone.text == "" || _Address.text == "" || _Email.text == "")
            {
                _NextStep.interactable = false;
            }
            else
            {
                _NextStep.interactable = true;
            }
        }
        if (step == 1)
        {
            if (bank == "")
            {
                _NextStep.interactable = false;
            }
            else
            {
                _NextStep.interactable = true;
            }
        }
    }

    private void NextStepAction()
    {
        if (step == 0)
        {
            step++;
        }
        else if (step == 1)
        {
            StartCoroutine(ConfirmPayCourotine());
        }
        else
        {
            toHomeState();
        }
    }

    private void BankAction(string bankName)
    {
        bank = bankName;

        if (bank == "BNI")
        {
            _NoRek.text = "1234567890";
            _AtasNama.text = "Indah Nursantie BNI";
        }
        if (bank == "MANDIRI")
        {
            _NoRek.text = "0987654321";
            _AtasNama.text = "Indah Nursantie MANDIRI";
        }
    }

    private void ConfirmPay()
    {
        StartCoroutine(ConfirmPayCourotine());
    }

    private IEnumerator ConfirmPayCourotine()
    {
        WWW confirmPay = GetPostData.ConfirmPay(_Name.text, _Phone.text, _Address.text, _Email.text, totalPayment, bank);
        yield return confirmPay;
        Debug.Log(confirmPay.text);

        step++;
    }

    public override void CloseModal()
    {
        _NextStep.onClick.RemoveAllListeners();
        _BNI.onClick.RemoveAllListeners();
        _MANDIRI.onClick.RemoveAllListeners();

        _Back.onClick.RemoveAllListeners();

        base.CloseModal();
    }
}
