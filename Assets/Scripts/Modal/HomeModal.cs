﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BaseApps;
using SimpleJSON;

public class HomeModal : BaseModal
{
    [SerializeField]
    private Button _MyAccount;
    [SerializeField]
    private Button _CS;
    [SerializeField]
    private RectTransform _ImageHolder;

    private int imagePostion = 0;

    //public UnityAction toLoginState;
    public UnityAction toARState;
    public UnityAction toAkunState;

    private List<ItemClass> _ItemClassList;

    private static HomeModal _Instance;
    public static HomeModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<HomeModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no HomeModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        Singleton.Instance.Decoration = TypeDecoration.NullDecoration;
        _MyAccount.onClick.AddListener(toAkunState);
        _CS.onClick.AddListener(() => Application.OpenURL(CONST_VAR.WHATSAPP));

        //_Logout.onClick.AddListener(toLoginState);
        imagePostion = 0;
        StartCoroutine(SetImagePosition());
        StartCoroutine(GetItemsCourotine());
    }

    public void ToARAction(int decoration)
    {
        if (decoration != 0)
        {
            Singleton.Instance.Decoration = StaticFunction.ConvertTypeIntToDecoration(decoration);
            toARState();
        }
        else
        {
            Debug.LogError("Type Decoration is Null");
        }
    }

    private IEnumerator SetImagePosition()
    {
        yield return new WaitForSeconds(5f);
        if (imagePostion < 4)
        {
            imagePostion++;
        }
        else
        {
            imagePostion = 0;
        }
        StartCoroutine(SetImagePosition());
    }

    private IEnumerator GetItemsCourotine()
    {
        WWW wwwItem = new WWW(CONST_VAR.GET_ITEM);
        yield return wwwItem;        
        string jsonString = wwwItem.text;

        GetItems cartJSON = new GetItems(jsonString);
        _ItemClassList = cartJSON.ParseJSON();
        
        foreach (ItemClass itemClass in _ItemClassList)
        {
            ItemProductClass item = itemClass as ItemProductClass;
            Singleton.Instance.ListProduct.Add(item);
            yield return new WaitForEndOfFrame();
        }
    }

    protected override void Loop()
    {
        base.Loop();

        float anchoredX = Mathf.Lerp(_ImageHolder.anchoredPosition.x, -720 * imagePostion, Time.deltaTime * 8);
        _ImageHolder.anchoredPosition = new Vector2(anchoredX, _ImageHolder.anchoredPosition.y);
    }
    
    public override void CloseModal()
    {
        _MyAccount.onClick.RemoveAllListeners();
        _CS.onClick.RemoveAllListeners();
        base.CloseModal();
    }
}
