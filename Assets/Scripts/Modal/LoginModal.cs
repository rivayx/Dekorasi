﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Events;

public class LoginModal : BaseModal
{
    [SerializeField]
    private InputField _Username;
    [SerializeField]
    private InputField _Password;
    [SerializeField]
    private Toggle _Remember;
    [SerializeField]
    private Button _Login;
    [SerializeField]
    private Button _Register;

    public UnityAction toHomeState;
    public UnityAction toRegisterState;

    private static LoginModal _Instance;
    public static LoginModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<LoginModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no LoginModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _Login.onClick.AddListener(LoginAction);
        _Register.onClick.AddListener(toRegisterState);
        _Username.text = "";
        _Password.text = "";
        _Remember.isOn = false;

        Singleton.Instance.Username = "";
        Singleton.Instance.FullName = "";
        Singleton.Instance.Email = "";
        Singleton.Instance.Phone = "";

        if (EncryptedPlayerPrefs.GetString("username") != "")
        {
            _Username.text = EncryptedPlayerPrefs.GetString("username");
            _Password.text = EncryptedPlayerPrefs.GetString("password");
            _Remember.isOn = true;
        }
    }

    private void LoginAction()
    {
        if (_Username.text == "" || _Password.text == "")
        {
            Debug.Log("Username dan Password tidak boleh kosong");
            MessageModal.Instance().OpenMessage("Username dan Password tidak boleh kosong");
        }
        else
        {
            StartCoroutine(LoginCourotine());
        }
    }

    private IEnumerator LoginCourotine()
    {
        WWW getUser = GetPostData.GetUser(_Username.text, _Password.text);
        yield return getUser;
        string jsonString = getUser.text;
        //Debug.Log(jsonString);
        JSONNode node = JSONNode.Parse(jsonString);
        string message = node["message"];
        if (message == "Login Berhasil")
        {
            JSONNode nodeData = node["data"];
            Singleton.Instance.Username = nodeData["username"];
            Singleton.Instance.FullName = nodeData["name"];
            Singleton.Instance.Address = nodeData["address"];
            Singleton.Instance.Email = nodeData["email"];
            Singleton.Instance.Phone = nodeData["phone"];

            if (_Remember.isOn)
            {
                EncryptedPlayerPrefs.SetString("username", _Username.text);
                EncryptedPlayerPrefs.SetString("password", _Password.text);
                _Remember.isOn = false;
            }
            else
            {
                EncryptedPlayerPrefs.DeleteKey("username");
                EncryptedPlayerPrefs.DeleteKey("password");
            }

            _Username.text = "";
            _Password.text = "";
            MessageModal.Instance().OpenMessage(message, toHomeState);
        }
        else
        {
            Debug.Log(message);
            MessageModal.Instance().OpenMessage(message);
        }
    }

    public override void CloseModal()
    {
        _Login.onClick.RemoveAllListeners();
        _Register.onClick.RemoveAllListeners();
        base.CloseModal();
    }
}
