﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RegisterModal : BaseModal
{
    [SerializeField]
    private InputField _Username;
    [SerializeField]
    private InputField _Password;
    [SerializeField]
    private InputField _Name;
    [SerializeField]
    private InputField _Address;
    [SerializeField]
    private InputField _Email;
    [SerializeField]
    private InputField _Phone;

    [SerializeField]
    private Button _Register;
    [SerializeField]
    private Button _BackButton;

    public UnityAction toLoginState;

    private static RegisterModal _Instance;
    public static RegisterModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<RegisterModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no RegisterModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _Register.onClick.AddListener(RegisterUserAction);
        _BackButton.onClick.AddListener(toLoginState);
    }

    private void RegisterUserAction()
    {
        if (_Username.text == "" || _Password.text == "" || _Name.text == "" || _Address.text == "" || _Email.text == "" || _Phone.text == "")
        {
            Debug.Log("Lengkapi Data Anda");
            MessageModal.Instance().OpenMessage("Lengkapi Data Anda");
        }
        else
        {
            StartCoroutine(Register());
        }
    }

    private IEnumerator Register()
    {
        WWW addUser = GetPostData.RegisterUser(_Username.text, _Password.text, _Name.text, _Address.text, _Email.text, _Phone.text);
        yield return addUser;

        if (addUser.error == null)
        {
            Debug.Log(addUser.text);

            if (addUser.text == "Duplicate entry '" + _Username.text + "' for key 'PRIMARY'")
            {
                Debug.Log("Username sudah terdaftar, silahkan gunakan username lain");
                MessageModal.Instance().OpenMessage("Username sudah terdaftar, silahkan gunakan username lain");
            }
            else
            {
                if (addUser.text == "Berhasil Menambahkan User, Silahkan Login")
                {
                    _Username.text = "";
                    _Password.text = "";
                    _Name.text = "";
                    _Address.text = "";
                    _Email.text = "";
                    _Phone.text = "";

                    MessageModal.Instance().OpenMessage(addUser.text, toLoginState);
                }
                else
                {
                    MessageModal.Instance().OpenMessage(addUser.text);
                }
            }
        }
    }

    public override void CloseModal()
    {
        _BackButton.onClick.RemoveAllListeners();
        _Register.onClick.RemoveAllListeners();
        base.CloseModal();
    }
}
