﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class ARState : FSMState
{
    private AsyncOperation _LoadAR;
    private bool bSceneLoaded;

    public ARState()
    {
        stateID = StateID.AR;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter ARState");
        bSceneLoaded = false;

        if (SceneManager.GetActiveScene().name != "ARInstantTracker")
            _LoadAR = SceneManager.LoadSceneAsync("ARInstantTracker");
        else
            EnterARState();

        base.OnEnter();
    }

    private void EnterARState()
    {
        ARModal arModal = ARModal.Instance();
        arModal.toHomeState = ToHomeState;
        arModal.toCheckoutState = ToCheckoutState;
        arModal.OpenModal();
        bSceneLoaded = true;
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    private void ToCheckoutState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_CHECKOUT);
    }

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadAR.isDone)
            {
                EnterARState();
            }
            else
            {
                MessageModal.Instance().Loading(_LoadAR.progress);
                //Debug.Log(_LoadAR.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave ARState");
        ARModal arModal = ARModal.Instance();
        arModal.CloseModal();
        arModal.toHomeState = null;
        arModal.toCheckoutState = null;

        base.OnLeave();
    }
}