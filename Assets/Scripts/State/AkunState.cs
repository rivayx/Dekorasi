﻿using BaseApps;
using UnityEngine;

public class AkunState : FSMState
{
    public AkunState()
    {
        stateID = StateID.ACCOUNT;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter AkunState");

        AkunModal akunModal = AkunModal.Instance();
        akunModal.OpenModal();
        akunModal.RegisterModal(ToHomeState);

        base.OnEnter();
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    public override void OnLeave()
    {
        Debug.Log("Leave AkunState");

        AkunModal akunModal = AkunModal.Instance();
        akunModal.CloseModal();
        base.OnLeave();
    }
}
