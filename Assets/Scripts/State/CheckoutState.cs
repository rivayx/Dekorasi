﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class CheckoutState : FSMState
{
    private AsyncOperation _LoadUI;
    private bool bSceneLoaded;

    public CheckoutState()
    {
        stateID = StateID.CHECKOUT;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter CheckoutState");
        bSceneLoaded = false;

        if (SceneManager.GetActiveScene().name != "UI")
            _LoadUI = SceneManager.LoadSceneAsync("UI");
        else
            EnterCheckoutState();

        base.OnEnter();
    }

    private void EnterCheckoutState()
    {
        CheckoutModal checkoutModal = CheckoutModal.Instance();
        checkoutModal.toARState = ToARState;
        checkoutModal.toHomeState = ToHomeState;
        checkoutModal.OpenModal();
        bSceneLoaded = true;
    }

    private void ToARState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_AR);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadUI.isDone)
            {
                EnterCheckoutState();
            }
            else
            {
                MessageModal.Instance().Loading(_LoadUI.progress);
                //Debug.Log(_LoadHome.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave CheckoutState");

        CheckoutModal checkoutModal = CheckoutModal.Instance();
        checkoutModal.toARState = null;
        checkoutModal.CloseModal();

        base.OnLeave();
    }
}
