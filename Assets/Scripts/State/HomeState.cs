﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class HomeState : FSMState
{
    private AsyncOperation _LoadUI;
    private bool bSceneLoaded;

    public HomeState()
    {
        stateID = StateID.HOME;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter HomeState");
        bSceneLoaded = false;
        Singleton.Instance.ListProduct.Clear();

        if (SceneManager.GetActiveScene().name != "UI")
            _LoadUI = SceneManager.LoadSceneAsync("UI");
        else
            EnterHomeState();

        base.OnEnter();
    }

    private void EnterHomeState()
    {
        HomeModal homeModal = HomeModal.Instance();
        homeModal.toARState = ToARState;
        homeModal.toAkunState = ToAkunState;
        //homeModal.toLoginState = ToLoginState;
        homeModal.OpenModal();
        bSceneLoaded = true;
    }

    private void ToARState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_AR);
    }

    private void ToAkunState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_ACCOUNT);
    }

    //private void ToLoginState()
    //{
    //    BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
    //    appRuntime.SetTransition(Transition.TRANSITION_TO_LOGIN);
    //}

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadUI.isDone)
            {
                EnterHomeState();
            }
            else
            {
                MessageModal.Instance().Loading(_LoadUI.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave LoginState");

        HomeModal homeModal = HomeModal.Instance();
        homeModal.toARState = null;
        //homeModal.toLoginState = null;
        homeModal.CloseModal();

        base.OnLeave();
    }
}
