﻿using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class LoginState : FSMState
{
    private AsyncOperation _LoadUI;
    private bool bSceneLoaded;

    public LoginState()
    {
        stateID = StateID.LOGIN;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter LoginState");
        bSceneLoaded = false;

        //Debug.Log(SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name != "UI")
            _LoadUI = SceneManager.LoadSceneAsync("UI");
        else
            EnterLoginState();

        base.OnEnter();
    }

    private void EnterLoginState()
    {
        LoginModal loginModal = LoginModal.Instance();
        loginModal.toRegisterState = ToRegisterState;
        loginModal.toHomeState = ToHomeState;
        loginModal.OpenModal();
        bSceneLoaded = true;
    }

    private void ToRegisterState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_REGISTER);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadUI.isDone)
            {
                EnterLoginState();
            }
            else
            {
                //Debug.Log(_LoadHome.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave LoginState");

        LoginModal loginModal = LoginModal.Instance();
        loginModal.toHomeState = null;
        loginModal.toRegisterState = null;
        loginModal.CloseModal();

        base.OnLeave();
    }
}