﻿using BaseApps;
using UnityEngine;

public class RegisterState : FSMState
{
    public RegisterState()
    {
        stateID = StateID.REGISTER;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter RegisterState");

        RegisterModal registerModal = RegisterModal.Instance();
        registerModal.toLoginState = ToLoginState;
        registerModal.OpenModal();

        base.OnEnter();
    }

    private void ToLoginState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_LOGIN);
    }

    public override void Update()
    {

    }

    public override void OnLeave()
    {
        Debug.Log("Leave RegisterState");

        RegisterModal registerModal = RegisterModal.Instance();
        registerModal.toLoginState = null;
        registerModal.CloseModal();

        base.OnLeave();
    }
}
