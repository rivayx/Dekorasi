﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CartVariableGatherer : BaseVariableGatherer
{
    public Text ProductName;
    public Text Price;
    public Button Delete;
}
